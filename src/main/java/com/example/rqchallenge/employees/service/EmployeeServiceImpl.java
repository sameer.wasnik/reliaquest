package com.example.rqchallenge.employees.service;

import com.example.rqchallenge.employees.dto.ResponseDto;
import com.example.rqchallenge.employees.dto.EmployeeListResponseDto;
import com.example.rqchallenge.employees.dto.EmployeeResponseDto;
import com.example.rqchallenge.employees.exception.CustomException;
import com.example.rqchallenge.employees.model.Employee;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.example.rqchallenge.employees.constants.Constants.*;

@Service
public class EmployeeServiceImpl implements EmployeeService{

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    RestTemplate restTemplate;

    @Override
    public List<Employee> getAllEmployees() throws CustomException {

        String url = BASE_URL + VERSION1 + GET_ALL_EMPLOYEES;
        LOGGER.info("getAllEmployees: url: " + url);

        List<Employee> employeeList = new ArrayList<>();
//        HttpHeaders httpHeaders;
        HttpEntity<String> requestEntity = null;
        ResponseEntity<EmployeeListResponseDto> response = null;

        try {
            response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, EmployeeListResponseDto.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("getAllEmployees: Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }
            if (response.getBody() != null) {
                employeeList = response.getBody().getData();
                LOGGER.info("getAllEmployees: Response: " + employeeList);
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("getAllEmployees: Exception: " + e.getMessage());
            //Can send 503 custom message, instead of actual 3rd party status
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("getAllEmployees: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return employeeList;

    }

    @Override
    public List<Employee> getEmployeesByNameSearch(String searchString) throws CustomException {

        String url = BASE_URL + VERSION1 + GET_ALL_EMPLOYEES;

        HttpEntity<String> requestEntity = null;
        ResponseEntity<EmployeeListResponseDto> response = null;
        List<Employee> employeeList = new ArrayList<>();

        try {
            response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, EmployeeListResponseDto.class);
            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("getEmployeesByNameSearch: Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }

            EmployeeListResponseDto employeeListResponseDto = response.getBody();
            if (employeeListResponseDto != null) {
                employeeList =
                    employeeListResponseDto.getData()
                        .stream()
                        .filter(e -> e.getEmployeeName().contains(searchString))
                        .collect(Collectors.toList());

                LOGGER.info("getEmployeesByNameSearch: " + searchString + ": " + employeeList);
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("getEmployeesByNameSearch: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("getEmployeesByNameSearch: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return employeeList;
    }

    @Override
    public Employee getEmployeeById(String id) throws CustomException {

        String url = BASE_URL + VERSION1 + GET_EMPLOYEE_BY_ID;

        HttpEntity<String> requestEntity = null;
        ResponseEntity<EmployeeResponseDto> response = null;
        Employee employee = null;

        try {
            response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, EmployeeResponseDto.class, id);
            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("getEmployeeById: Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }
            if (response.getBody() != null && response.getBody().getData() != null) {
                employee = response.getBody().getData();
                LOGGER.info("getEmployeeById: " + employee);
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("getEmployeeById: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("getEmployeeById: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return employee;
    }

    @Override
    public Integer getHighestSalaryOfEmployees() throws CustomException {

        String url = BASE_URL + VERSION1 + GET_ALL_EMPLOYEES;

        HttpEntity<String> requestEntity = null;
        ResponseEntity<EmployeeListResponseDto> response = null;
        Integer highestSalary = 0;

        try {
            response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, EmployeeListResponseDto.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("getHighestSalaryOfEmployees: Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }

            EmployeeListResponseDto employeeListResponseDto = response.getBody();

            if (employeeListResponseDto != null) {
                highestSalary =
                    Integer.valueOf(employeeListResponseDto.getData()
                        .stream()
                        .max(Comparator.comparingInt(e -> Integer.parseInt(e.getEmployeeSalary())))
                        .get()
                        .getEmployeeSalary());

                LOGGER.info("getHighestSalaryOfEmployees: " + highestSalary);
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("getHighestSalaryOfEmployees: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("getHighestSalaryOfEmployees: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return highestSalary;
    }

    @Override
    public List<String> getTopTenHighestEarningEmployeeNames() throws CustomException {

        String url = BASE_URL + VERSION1 + GET_ALL_EMPLOYEES;

        HttpEntity<String> requestEntity = null;
        ResponseEntity<EmployeeListResponseDto> response = null;
        List<String> employeeList = new ArrayList<>();

        try {
            response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, EmployeeListResponseDto.class);

            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("getTopTenHighestEarningEmployeeNames: Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }

            EmployeeListResponseDto employeeListResponseDto = response.getBody();
            if (employeeListResponseDto != null) {
                employeeListResponseDto.getData()
                    .stream()
                    .sorted((e1, e2) -> Integer.compare(Integer.parseInt(e2.getEmployeeSalary()), Integer.parseInt(e1.getEmployeeSalary())))
                    .limit(10)
                    .forEach(e1 -> employeeList.add(e1.getEmployeeName()));

                LOGGER.info("getTopTenHighestEarningEmployeeNames: " + employeeList);
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("getTopTenHighestEarningEmployeeNames: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("getTopTenHighestEarningEmployeeNames: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return employeeList;
    }

    @Override
    public Employee createEmployee(Map<String, Object> employeeInput) throws CustomException {

        String url = BASE_URL + VERSION1 + CREATE_EMPLOYEE;
        HttpEntity<Map<String, Object>> requestEntity = new HttpEntity(employeeInput, new HttpHeaders());
        ResponseEntity<ResponseDto> response = null;
        Employee employee = null;

        try {
            response = restTemplate.exchange(url, HttpMethod.POST, requestEntity, ResponseDto.class);
            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("createEmployee: Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }

            ResponseDto responseDto = response.getBody();
            if (responseDto != null) {
                employee = new Employee();
                employee.setId((Integer) responseDto.getData().get(EMPLOYEE_ID));
                employee.setEmployeeName((String) responseDto.getData().get(EMPLOYEE_NAME));
                employee.setEmployeeAge(Integer.parseInt((String) responseDto.getData().get(EMPLOYEE_AGE)));
                employee.setEmployeeSalary((String) responseDto.getData().get(EMPLOYEE_SAlARY));
                LOGGER.info("createEmployee: " + employee);
            }

        } catch (HttpStatusCodeException e) {
            LOGGER.error("createEmployee: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("createEmployee: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
        return employee;
    }

    @Override
    public String deleteEmployeeById(String id) throws CustomException {
        //Get employee details which will be deleted
        String url = BASE_URL + VERSION1 + GET_EMPLOYEE_BY_ID;
        String employeeName = "";

        HttpEntity<String> requestEntity = null;
        ResponseEntity<EmployeeResponseDto> response = null;

        try {
            response = restTemplate.exchange(url, HttpMethod.GET, requestEntity, EmployeeResponseDto.class, id);

            if (response.getStatusCode() != HttpStatus.OK) {
                LOGGER.error("deleteEmployeeById: getEmployee: Employee Not Found Response: " + response.getBody());
                throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("deleteEmployeeById: getEmployee: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        }

        try {
            EmployeeResponseDto employeeResponseDto = response.getBody();
            if (employeeResponseDto != null && employeeResponseDto.getData() != null) {

                employeeName = employeeResponseDto.getData().getEmployeeName();

                url = BASE_URL + VERSION1 + DELETE_EMPLOYEE;
                ResponseEntity<String> deleteResponse = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, String.class, id);

                if (deleteResponse.getStatusCode() != HttpStatus.OK) {
                    LOGGER.error("deleteEmployeeById: getEmployee: Response: " + response.getBody());
                    throw new CustomException(response.getStatusCode(), "Error occurred: " + url);
                } else {
                    LOGGER.info("deleteEmployeeById: Deleted: id:" + id + " name: " + employeeName);
                }
            }
        } catch (HttpStatusCodeException e) {
            LOGGER.error("deleteEmployeeById: Exception: " + e.getMessage());
            throw new CustomException(e.getStatusCode(), e.getMessage());
        } catch (Exception e) {
            LOGGER.error("getEmployeeById: Exception: " + e.getMessage());
            throw new CustomException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        return employeeName;
    }
}
