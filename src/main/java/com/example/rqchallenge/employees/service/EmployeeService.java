package com.example.rqchallenge.employees.service;

import com.example.rqchallenge.employees.exception.CustomException;
import com.example.rqchallenge.employees.model.Employee;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import java.util.List;
import java.util.Map;

public interface EmployeeService {

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    List<Employee> getAllEmployees() throws CustomException;

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    List<Employee> getEmployeesByNameSearch(String searchString) throws CustomException;

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    Employee getEmployeeById(String id) throws CustomException;

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    Integer getHighestSalaryOfEmployees() throws CustomException;

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    List<String> getTopTenHighestEarningEmployeeNames() throws CustomException;

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    Employee createEmployee(Map<String, Object> employeeInput) throws CustomException;

    @Retryable(value = Exception.class, maxAttemptsExpression = "${retry.maxAttempts}", backoff = @Backoff(delayExpression = "${retry.maxDelay}"))
    String deleteEmployeeById(String id) throws CustomException;
}
