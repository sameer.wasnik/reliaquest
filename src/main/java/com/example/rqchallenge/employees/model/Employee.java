package com.example.rqchallenge.employees.model;


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Employee {

    private int id;

    @JsonProperty("employee_name")
    private String employeeName;

    @JsonProperty("employee_salary")
    private String employeeSalary;

    @JsonProperty("employee_age")
    private int employeeAge;

    @JsonProperty("profile_image")
    private String profileImage;

    public Employee() {
    }

    public Employee(int id, String employeeName, String employeeSalary, int employeeAge, String profileImage) {
        this.id = id;
        this.employeeName = employeeName;
        this.employeeSalary = employeeSalary;
        this.employeeAge = employeeAge;
        this.profileImage = profileImage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return id == employee.id && employeeAge == employee.employeeAge && employeeName.equals(employee.employeeName) && employeeSalary.equals(employee.employeeSalary);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, employeeName, employeeSalary, employeeAge);
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSalary() {
        return employeeSalary;
    }

    public void setEmployeeSalary(String employeeSalary) {
        this.employeeSalary = employeeSalary;
    }

    public int getEmployeeAge() {
        return employeeAge;
    }

    public void setEmployeeAge(int employeeAge) {
        this.employeeAge = employeeAge;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @Override
    public String toString() {
        return "Employee{" +
            "id=" + id +
            ", employeeName='" + employeeName + '\'' +
            ", employeeSalary='" + employeeSalary + '\'' +
            ", employeeAge='" + employeeAge + '\'' +
            ", profileImage='" + profileImage + '\'' +
            '}';
    }
}
