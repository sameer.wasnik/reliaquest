package com.example.rqchallenge.employees.controller;

import com.example.rqchallenge.employees.exception.CustomException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class EmployeeControllerAdvice {
    @ExceptionHandler(CustomException.class)
    public final ResponseEntity<String> handleException(CustomException customException) {
        return new ResponseEntity<>(customException.getMessage(),customException.getHttpStatus());
    }
}
