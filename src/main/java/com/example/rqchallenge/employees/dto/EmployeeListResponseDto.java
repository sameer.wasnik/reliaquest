package com.example.rqchallenge.employees.dto;

import com.example.rqchallenge.employees.model.Employee;

import java.util.List;

public class EmployeeListResponseDto {
    private String status;
    private String message;
    private List<Employee> data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Employee> getData() {
        return data;
    }

    public void setData(List<Employee> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "EmployeeListResponseDto{" +
            "status='" + status + '\'' +
            ", message='" + message + '\'' +
            ", data=" + data +
            '}';
    }
}
