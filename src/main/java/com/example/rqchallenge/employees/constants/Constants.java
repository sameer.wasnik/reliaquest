package com.example.rqchallenge.employees.constants;

public class Constants {

    public static final String EMPLOYEE_ID = "id";
    public static final String EMPLOYEE_NAME = "name";
    public static final String EMPLOYEE_SAlARY = "salary";
    public static final String EMPLOYEE_AGE = "age";

    public static final String BASE_URL = "https://dummy.restapiexample.com";
    public static final String VERSION1 = "/api/v1";
    public static final String GET_ALL_EMPLOYEES = "/employees";
    public static final String GET_EMPLOYEE_BY_ID = "/employee/{id}";
    public static final String CREATE_EMPLOYEE = "/create";
    public static final String DELETE_EMPLOYEE = "/delete/{id}";
}
