package com.example.rqchallenge.employees.service;

import com.example.rqchallenge.employees.dto.EmployeeListResponseDto;
import com.example.rqchallenge.employees.dto.EmployeeResponseDto;
import com.example.rqchallenge.employees.dto.ResponseDto;
import com.example.rqchallenge.employees.exception.CustomException;
import com.example.rqchallenge.employees.model.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.*;

import static com.example.rqchallenge.employees.constants.Constants.*;

public class EmployeeServiceImplTest {

    @InjectMocks
    EmployeeServiceImpl employeeServiceImpl;

    @Mock
    private RestTemplate restTemplate;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    private Employee getEmployee1() {
        return new Employee(1,"Test Employee 1","230000",20,null);
    }
    private Employee getEmployee2() {
        return new Employee(2,"Test Employee 2","340000",30,null);
    }
    private Employee getEmployee3() {
        return new Employee(3,"Dummy Employee 3","120000",10,null);
    }

    private List<Employee> getEmployeeList() {
        List<Employee> employeeList = new ArrayList<Employee>() {{
            add(getEmployee1());
            add(getEmployee2());
            add(getEmployee3());
        }};
        return employeeList;
    }

    private List<String> getEmployeeSortedSalaryList() {
        List<String> employeeNames = new ArrayList<String>() {{
            add(getEmployee2().getEmployeeName());
            add(getEmployee1().getEmployeeName());
            add(getEmployee3().getEmployeeName());
        }};
        return employeeNames;
    }

    @Test
    public void testGetAllEmployees_success() throws Exception {
        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenReturn(responseEntity);

        List<Employee> employeeList = employeeServiceImpl.getAllEmployees();
        Assertions.assertIterableEquals(employeeList,employeeListResponseDto.getData());
    }

    @Test
    public void testGetAllEmployees_failure() throws Exception {
        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<EmployeeListResponseDto>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenThrow(RestClientException.class);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.getAllEmployees());
    }

    @Test
    public void testGetEmployeesByNameSearch() throws Exception {
        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenReturn(responseEntity);

        List<Employee> employeeList = employeeServiceImpl.getEmployeesByNameSearch("Test");
        Assertions.assertIterableEquals(employeeList,employeeListResponseDto.getData().subList(0,2));
    }

    @Test
    public void testGetEmployeesByNameSearch_failure() throws Exception {
        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<EmployeeListResponseDto>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenThrow(RestClientException.class);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.getEmployeesByNameSearch(""));
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        employeeResponseDto.setData(getEmployee1());
        employeeResponseDto.setStatus("success");
        ResponseEntity<EmployeeResponseDto> responseEntity =
            new ResponseEntity<>(employeeResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeResponseDto>>any(),
                Optional.ofNullable(ArgumentMatchers.any())))
            .thenReturn(responseEntity);

        Employee employee = employeeServiceImpl.getEmployeeById("1");
        Assertions.assertEquals(employee,employeeResponseDto.getData());
    }

    @Test
    public void testGetEmployeeById_failure() throws Exception {
        EmployeeResponseDto employeeResponseDto = new EmployeeResponseDto();
        employeeResponseDto.setData(getEmployee1());
        employeeResponseDto.setStatus("success");
        ResponseEntity<EmployeeResponseDto> responseEntity =
            new ResponseEntity<>(employeeResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenThrow(RestClientException.class);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.getEmployeeById("1"));
    }

    @Test
    public void testGetHighestSalaryOfEmployees() throws Exception {

        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenReturn(responseEntity);

        Integer highestSalary = employeeServiceImpl.getHighestSalaryOfEmployees();
        Assertions.assertEquals(highestSalary,Integer.parseInt(getEmployee2().getEmployeeSalary()));
    }

    @Test
    public void testGetHighestSalaryOfEmployees_failure() throws Exception {
        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<EmployeeListResponseDto>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenThrow(RestClientException.class);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.getHighestSalaryOfEmployees());
    }

    @Test
    public void testGetTopTenHighestEarningEmployeeNames() throws Exception {

        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenReturn(responseEntity);

        List<String> employeeList = employeeServiceImpl.getTopTenHighestEarningEmployeeNames();
        List<String> expectedResult = getEmployeeSortedSalaryList();

        Assertions.assertIterableEquals(employeeList, expectedResult);
    }

    @Test
    public void testGetTopTenHighestEarningEmployeeNames_failure() throws Exception {
        EmployeeListResponseDto employeeListResponseDto = new EmployeeListResponseDto();
        employeeListResponseDto.setData(getEmployeeList());
        employeeListResponseDto.setStatus("success");
        ResponseEntity<EmployeeListResponseDto> responseEntity =
            new ResponseEntity<EmployeeListResponseDto>(employeeListResponseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeListResponseDto>>any()))
            .thenThrow(RestClientException.class);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.getTopTenHighestEarningEmployeeNames());
    }

    @Test
    public void  testCreateEmployee() throws Exception {
        Map<String, Object> employeInput = new HashMap<String, Object>() {{
            put("name","Test Employee 1");
            put("age","20");
            put("salary","230000");
            put("id",1);
        }};
        ResponseDto responseDto = new ResponseDto();
        responseDto.setData(employeInput);
        ResponseEntity<ResponseDto> responseEntity =
            new ResponseEntity<>(responseDto, HttpStatus.OK);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.POST),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<ResponseDto>>any()))
            .thenReturn(responseEntity);

        Employee employee = employeeServiceImpl.createEmployee(employeInput);
        Assertions.assertEquals(employee,getEmployee1());
    }

    @Test
    public void testCreateEmployee_failure() throws Exception {
        Map<String, Object> employeInput = new HashMap<String, Object>() {{
            put("name","Test Employee 1");
            put("age","20");
            put("salary","230000");
            put("id",1);
        }};
        ResponseDto responseDto = new ResponseDto();
        responseDto.setData(employeInput);
        //Not found error
        ResponseEntity<ResponseDto> responseEntity =
            new ResponseEntity<>(responseDto, HttpStatus.NOT_FOUND);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.POST),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<ResponseDto>>any()))
            .thenReturn(responseEntity);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.createEmployee(employeInput));
    }

    @Test
    public void  testDeleteEmployeeById() throws Exception {

        ResponseEntity<EmployeeResponseDto> responseEntity =
            new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.GET),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeResponseDto>>any(),
                Optional.ofNullable(ArgumentMatchers.any())))
            .thenReturn(responseEntity);

        Mockito.when(restTemplate.exchange(
                ArgumentMatchers.anyString(),
                ArgumentMatchers.eq(HttpMethod.DELETE),
                ArgumentMatchers.any(),
                ArgumentMatchers.<Class<EmployeeResponseDto>>any(),
                Optional.ofNullable(ArgumentMatchers.any())))
            .thenReturn(responseEntity);

        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.deleteEmployeeById("234324324"));

    }

    @Test
    public void testDeleteEmployeeById_failure() throws Exception {



        Assertions.assertThrows(CustomException.class,()->employeeServiceImpl.getAllEmployees());
    }
}
