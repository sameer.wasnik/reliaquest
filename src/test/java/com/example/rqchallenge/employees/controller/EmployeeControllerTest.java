package com.example.rqchallenge.employees.controller;

import ch.qos.logback.core.net.ObjectWriter;
import com.example.rqchallenge.employees.exception.CustomException;
import com.example.rqchallenge.employees.model.Employee;
import com.example.rqchallenge.employees.service.EmployeeService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;


import java.util.*;

import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(MockitoJUnitRunner.class)
public class EmployeeControllerTest {

    private MockMvc mvc;

    @InjectMocks
    private EmployeeController employeeController;

    @Mock
    private EmployeeService employeeService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(employeeController).setControllerAdvice(new EmployeeControllerAdvice()).build();
    }

    private Employee getEmployee1() {
        return new Employee(1,"Test Employee 1","230000",20,null);
    }
    private Employee getEmployee2() {
        return new Employee(2,"Test Employee 2","340000",30,null);
    }
    private Employee getEmployee3() {
        return new Employee(3,"Dummy Employee 3","120000",10,null);
    }

    private List<Employee> getEmployeeList() {
        List<Employee> employeeList = new ArrayList<Employee>() {{
            add(getEmployee1());
            add(getEmployee2());
            add(getEmployee3());
        }};
        return employeeList;
    }

    private List<String> getEmployeeSortedSalaryList() {
        List<String> employeeNames = new ArrayList<String>() {{
            add(getEmployee2().getEmployeeName());
            add(getEmployee1().getEmployeeName());
            add(getEmployee3().getEmployeeName());
        }};
        return employeeNames;
    }

    @Test
    public void testGetAllEmployees_success() throws Exception {
        Mockito.when(employeeService.getAllEmployees()).thenReturn(getEmployeeList());
        mvc.perform(MockMvcRequestBuilders
                .get("/api/v1/")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0].employee_name").value("Test Employee 1"))
            .andExpect(jsonPath("$[1].employee_age").value(30))
            .andExpect(jsonPath("$[2].employee_salary").value("120000"));

    }
    
    @Test
    public void testGetAllEmployees_failure() throws Exception {
        Mockito.when(employeeService.getAllEmployees()).thenThrow(new CustomException(HttpStatus.TOO_MANY_REQUESTS, "Too Many Request"));
        mvc.perform(MockMvcRequestBuilders
                .get("/api/v1/")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().is4xxClientError())
            .andExpect(result -> assertTrue(result.getResolvedException() instanceof CustomException));

    }

    @Test
    public void testGetEmployeesByNameSearch() throws Exception {
        Mockito.when(employeeService.getEmployeesByNameSearch("Dummy")).thenReturn(Collections.singletonList(getEmployee3()));
        mvc.perform(MockMvcRequestBuilders
                .get("/api/v1/search/Dummy")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(1)))
            .andExpect(jsonPath("$[0].employee_name").value("Dummy Employee 3"));

    }

    @Test
    public void testGetEmployeeById() throws Exception {
        Mockito.when(employeeService.getEmployeeById("1")).thenReturn(getEmployee1());
        mvc.perform(MockMvcRequestBuilders
                .get("/api/v1/1")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.employee_name").value("Test Employee 1"));

    }

    @Test
    public void testGetHighestSalaryOfEmployees() throws Exception {
        Mockito.when(employeeService.getHighestSalaryOfEmployees()).thenReturn(123);
        mvc.perform(MockMvcRequestBuilders
                .get("/api/v1/highestSalary")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value(123));
    }

    @Test
    public void testGetTopTenHighestEarningEmployeeNames() throws Exception {
        Mockito.when(employeeService.getTopTenHighestEarningEmployeeNames()).thenReturn(getEmployeeSortedSalaryList());
        mvc.perform(MockMvcRequestBuilders
                .get("/api/v1/topTenHighestEarningEmployeeNames")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$", hasSize(3)))
            .andExpect(jsonPath("$[0]").value("Test Employee 2"))
            .andExpect(jsonPath("$[1]").value("Test Employee 1"))
            .andExpect(jsonPath("$[2]").value("Dummy Employee 3"));

    }

    @Test
    public void testCreateEmployee() throws Exception {

        Map<String, Object> employeInput = new HashMap<String, Object>() {{
            put("name","Test Employee 1");
            put("age","20");
            put("salary","230000");
            put("id",1);
        }};

        ObjectMapper objectMapper = new ObjectMapper();;

        Mockito.when(employeeService.createEmployee(employeInput)).thenReturn(getEmployee1());
        mvc.perform(MockMvcRequestBuilders
                .post("/api/v1/")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employeInput)))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.employee_name").value("Test Employee 1"));

    }

    @Test
    public void testCreateEmployee_badRequest() throws Exception {
        Mockito.when(employeeService.createEmployee(ArgumentMatchers.any())).thenReturn(getEmployee1());
        mvc.perform(MockMvcRequestBuilders
                .post("/api/v1/")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isBadRequest());

    }

    @Test
    public void testDeleteEmployeeById() throws Exception {
        Mockito.when(employeeService.deleteEmployeeById("1")).thenReturn("Test Employee 1");
        mvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/1")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$").value( "Test Employee 1"));

    }

    @Test
    public void testDeleteEmployeeById_NotFound() throws Exception {
        Mockito.when(employeeService.deleteEmployeeById("234324324")).thenReturn("");
        mvc.perform(MockMvcRequestBuilders
                .delete("/api/v1/234324324")
                .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNotFound());

    }

}
